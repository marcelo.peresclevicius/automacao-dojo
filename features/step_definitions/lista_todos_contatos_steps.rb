Dado('que faço GET na API') do
    @response = HTTParty.get('https://api-de-tarefas.herokuapp.com/contacts')
end

Então('recebo uma lista de comandos') do
    @ret_code = @response.code
    @ret_body = @response.body
    
    raise "Valei me Deus, código de retorno não é 200! \n o código de retorno é: #{@ret_code}" if @ret_code != 200
    raise 'Valei me Deus, não está retornando nada na API !' if @ret_body['data'] == nil
end